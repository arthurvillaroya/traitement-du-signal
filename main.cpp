#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <math.h>
#include "FFT.hpp"

using namespace std;

#include "Wave.hpp"
#define MONO 1;
#define FREQSAMPLE 44100;

double charToDouble(unsigned char valueChar){
    return (double)((valueChar-127.5)/127.5); 
}

unsigned char doubleToChar(double valueDouble){
    if(valueDouble<-1.0) return (unsigned char)0 ;
    if(valueDouble>1.0) return (unsigned char)255 ;
    return (unsigned char)floor((valueDouble+1.0)*127.5);
}

void note(double note, double* dataDouble, long int data_nb, double frequence, float amplitude){
    double facteur = (2.0 * M_PI * note)/frequence;
    for(int i = 0; i < data_nb;i++){
		dataDouble[i] = sin(facteur*(double)i) * amplitude;
	}
}

void DFT(double *signal, double *partie_reelle, double *partie_imaginaire, int N){
    double dPi = (2.0*M_PI)/(double)N;
    for(int k = 0; k < N; k++){
        double KdPi = dPi * k;
        double reel = 0;
        double imaginaire = 0;
        for(int n = 0; n < N; n++){
            reel += signal[n] * cos(KdPi * n);
            imaginaire += signal[n] * sin(KdPi * n);  
        }
        imaginaire *= -1;
        partie_reelle[k] = reel;
        partie_imaginaire[k] = imaginaire;
    }
}

double maxFromTab(double *tab, int lenght){
    double max = tab[0];
    for(int i = 0; i < lenght; i++){
        if(max < tab[i]){max = tab[i];}
    }
    return max;
}

void calculNorme(double *norme, double *partie_reelle, double *partie_imaginaire, int N){
    for(int i = 0; i < N; i++){
        norme[i] = sqrt(partie_reelle[i]*partie_reelle[i] + partie_imaginaire[i]*partie_imaginaire[i]);
    }
    double max = maxFromTab(norme, N);
    for(int i = 0; i < N; i++){
        norme[i] /= max;
    }
}

void IDFT(double *signal, double *partie_reelle, double *partie_imaginaire, int N){
    double dPiN = (2*M_PI/N);
    double somme;
    for(int i = 0; i < N; i++){
        somme = 0;
        double dPiNi = dPiN * i;
        for(int k = 0; k < N; k++){
            somme += (partie_reelle[k] * cos(dPiNi * k) - partie_imaginaire[k] * sin(dPiNi * k))/N;
        }
        signal[i] = somme;
    }
}

void passeHaut(double* signalIF, double* signalRF, double *signalR, double *signalI, int freqC, int N, int freqE){
    int freqCN = (freqC*N)/freqE;

    for(int i = 0; i < freqCN; i++){
        signalIF[i] = 0;
        signalRF[i] = 0;
    }
    for(int i = freqCN; i < N-freqCN; i++){
        signalIF[i] = signalI[i];
        signalRF[i] = signalR[i];
    }
    for(int i = N-freqCN; i < N; i++){
        signalIF[i] = 0;
        signalRF[i] = 0;
    }
}

void passeBas(double* signalIF, double* signalRF, double *signalR, double *signalI, int freqC, int N, int freqE){
    int freqCN = (freqC*N)/freqE;

    for(int i = 0; i < freqCN; i++){
        signalIF[i] = signalI[i];
        signalRF[i] = signalR[i];
    }
    for(int i = freqCN; i < N-freqCN; i++){
        signalIF[i] = 0;
        signalRF[i] = 0;
    }
    for(int i = N-freqCN; i < N; i++){
        signalIF[i] = signalI[i];
        signalRF[i] = signalR[i];
    }
}

void coupeBande(double* signalIF, double* signalRF, double *signalR, double *signalI, int freqC1, int freqC2, int N, int freqE){
    int freqCN1 = (freqC1*N)/freqE;
    int freqCN2 = (freqC2*N)/freqE;

    for(int i = 0; i < freqCN1; i++){
        signalIF[i] = signalI[i];
        signalRF[i] = signalR[i];
    }
    for(int i = freqCN1; i < freqCN2; i++){
        signalIF[i] = 0;
        signalRF[i] = 0;
    }
    for(int i = freqCN2; i < N-freqCN2; i++){
        signalIF[i] = signalI[i];
        signalRF[i] = signalR[i];
    }
    for(int i = N-freqCN2; i < N-freqCN1; i++){
        signalIF[i] = 0;
        signalRF[i] = 0;
    }
    for(int i = N-freqCN1; i < N; i++){
        signalIF[i] = signalI[i];
        signalRF[i] = signalR[i];
    }
}

void passeBande(double* signalIF, double* signalRF, double *signalR, double *signalI, int freqC1, int freqC2, int N, int freqE){
    int freqCN1 = (freqC1*N)/freqE;
    int freqCN2 = (freqC2*N)/freqE;

    for(int i = 0; i < freqCN1; i++){
        signalIF[i] = 0;
        signalRF[i] = 0;
    }
    for(int i = freqCN1; i < freqCN2; i++){
        signalIF[i] = signalI[i];
        signalRF[i] = signalR[i];
    }
    for(int i = freqCN2; i < N-freqCN2; i++){
        signalIF[i] = 0;
        signalRF[i] = 0;
    }
    for(int i = N-freqCN2; i < N-freqCN1; i++){
        signalIF[i] = signalI[i];
        signalRF[i] = signalR[i];
    }
    for(int i = N-freqCN1; i < N; i++){
        signalIF[i] = 0;
        signalRF[i] = 0;
    }
}

void butter(double* signal, double* filtre, int N, double fe, double fc)
{
    double alpha = M_PI*fc/fe;
    double a[4], b[4];
    b[0] = alpha*alpha*alpha;
    b[3] = b[0];
    b[1] = 3.0* b[0];
    b[2] = b[1];

    double A = 1 + 2*alpha + 2*alpha*alpha + b[0];
    double B = -3 - 2*alpha + 2*alpha*alpha + b[1];
    double C = 3 - 2*alpha - 2*alpha*alpha + b[1];
    double D = -1 + 2*alpha - 2*alpha*alpha + b[0];

    a[0] = 0.0;
    a[1] = -B/A;
    a[2] = -C/A;
    a[3] = -D/A;

    b[0] /= A;
    b[1] /= A;
    b[2] /= A;
    b[3] /= A;

    for(int n = 0; n < N; n++){
        double _yn = 0;
        for(int k = 0; k<4; k++){
            int m = n - k;
            if(m >= 0) _yn += b[k] * signal[m] + a[k] * filtre[m];
        }
        filtre[n] = _yn;
    }
}

void transpo(double* signalIF, double* signalRF, double *signalR, double *signalI, int freqC, int N, int freqE){
    int freqCN = (freqC*N)/freqE;

    for(int i = 0; i < freqCN; i++){
        signalIF[i] = 0;
        signalRF[i] = 0;
    }
    for(int i = 0; i < N/2; i++){
        signalIF[i + freqCN] = signalI[i];
        signalRF[i + freqCN] = signalR[i];
    }
    for(int i = N/2; i < N; i++){
        signalIF[i - freqCN] = signalI[i];
        signalRF[i - freqCN] = signalR[i];
    }
}

void Exo1(){
    std::cout<<"Entrez le nom du fichier.wav : "<<std::endl;
    char sortie[10];
    std::cin>>sortie;
    std::cout<<std::endl;

    std::cout<<"Entrez la fréquence en Hz de la note : "<<std::endl;
    double frequenceNote;
    std::cin>>frequenceNote;
    std::cout<<std::endl;

    std::cout<<"Entrez la durée en secondes du signal : "<<std::endl;
    double temps;
    std::cin>>temps;
    std::cout<<std::endl;

    std::cout<<"Entrez l'amplitude du signal : "<<std::endl;
    double amplitude;
    std::cin>>amplitude;
    std::cout<<std::endl;

    std::cout<<"Enregistrement des paramètres du signal fait.."<<std::endl;

    short channels_nb = MONO;                          // Nombre de canaux (1 pour mono ou 2 pour stéréo)
    int sampling_freq = FREQSAMPLE;                    // Fréquence d'échantillonnage (en Hertz)(classique en musique : 44100 Hz)
    long int data_nb = sampling_freq * temps;          // Nombre de données
    unsigned char* data8 = new unsigned char[data_nb]; // Tableau de données

    double* dataDouble8 = new double[data_nb];
    std::cout<<"Création du tableau de donné en double"<<std::endl;
    note(frequenceNote, dataDouble8, data_nb, (double)sampling_freq, amplitude);
    std::cout<<"Stockage du signal"<<std::endl;

    for(int i = 0; i < data_nb; i++){
        data8[i] = doubleToChar(dataDouble8[i]);
    }

    Wave wave = Wave(data8, data_nb, channels_nb, sampling_freq);
    std::cout<<"Création du Signal"<<std::endl;

    wave.write(sortie);
    std::cout<<"Ecriture du fichier de fourier"<<std::endl;
}

void Exo2(){
    std::cout<<"Entrez le nom du entrant.wav : "<<std::endl;
    char entre[30];
    std::cin>>entre;
    std::cout<<std::endl;

    std::cout<<"Entrez le nom du sortantFourier.wav : "<<std::endl;
    char fourier[30];
    std::cin>>fourier;
    std::cout<<std::endl;

    std::cout<<"Entrez le nom du sortantInverse.wav : "<<std::endl;
    char inverseNom[30];
    std::cin>>inverseNom;
    std::cout<<std::endl;

    std::cout<<"Enregistrement des paramètres ..."<<std::endl;

    Wave entrant = Wave();
    entrant.read(entre);

    unsigned char *signalEntrant; 
    int N;
    int freq;
    entrant.getData8(& signalEntrant, & N, & freq);

    double entrantDouble[N];
    for(int i = 0; i < N; i++){
        entrantDouble[i] = charToDouble(signalEntrant[i]);
    }
    std::cout<<"Mise en mémoire des valeures doubles du signal entrant.."<<std::endl;

    double partie_reelle[N]; double partie_imaginaire[N];
    DFT(entrantDouble, partie_reelle, partie_imaginaire, N);
    double norme[N];
    double inverse[N];
    calculNorme(norme, partie_reelle, partie_imaginaire, N);
    IDFT(inverse, partie_reelle, partie_imaginaire, N);
    std::cout<<"Calcule de la norme et de l'inverse.."<<std::endl;

    unsigned char normeChar[N];
    unsigned char inverseChar[N];
    for(int i = 0; i < N; i++){
        normeChar[i] = doubleToChar(norme[i]);
        inverseChar[i] = doubleToChar(inverse[i]);
    }

    short channels_nb = MONO;                          // Nombre de canaux (1 pour mono ou 2 pour stéréo)
    int sampling_freq = freq;                    // Fréquence d'échantillonnage (en Hertz)(classique en musique : 44100 Hz)

    Wave sortantFourier = Wave(normeChar, N, channels_nb, sampling_freq);
    std::cout<<"Création du Signal de norme de fourier"<<std::endl;
    sortantFourier.write(fourier);
    std::cout<<"Ecriture du fichier de norme de fourier"<<std::endl;

    Wave inverseWave =  Wave(inverseChar, N, channels_nb, sampling_freq);
    std::cout<<"Création du Signal inverse de fourier"<<std::endl;
    inverseWave.write(inverseNom);
    std::cout<<"Ecriture du fichier inverse"<<std::endl;
}

void Exo2FFT(){
    std::cout<<"Entrez le nom du entrant.wav : "<<std::endl;
    char entre[30];
    std::cin>>entre;
    std::cout<<std::endl;

    std::cout<<"Entrez le nom du sortantFourier.wav : "<<std::endl;
    char fourier[30];
    std::cin>>fourier;
    std::cout<<std::endl;

    std::cout<<"Entrez le nom du sortantInverse.wav : "<<std::endl;
    char inverseNom[30];
    std::cin>>inverseNom;
    std::cout<<std::endl;

    std::cout<<"Enregistrement des paramètres ..."<<std::endl;

    Wave entrant = Wave();
    entrant.read(entre);

    unsigned char *signalEntrant; 
    int N;
    int freq;
    entrant.getData8(& signalEntrant, & N, & freq);
    int m = ceil(log2(N));
    int deuxPowm = pow(2,m);

    double partie_reelle[deuxPowm]; double partie_imaginaire[deuxPowm];
    for(int i = 0; i < N; i++){
        partie_reelle[i] = charToDouble(signalEntrant[i]);
        partie_imaginaire[i] = charToDouble(signalEntrant[i]);
    }
    for(int i = N; i < deuxPowm; i++){
        partie_reelle[i] = 0;
        partie_imaginaire[i] = 0;
    }
    std::cout<<"Mise en mémoire des valeures doubles du signal entrant.."<<std::endl;

    int fft = FFT(1, m, partie_reelle, partie_imaginaire);

    double norme[deuxPowm];
    calculNorme(norme, partie_reelle, partie_imaginaire, deuxPowm);

    fft = FFT(-1, m, partie_reelle, partie_imaginaire);

    std::cout<<"Calcule de la norme et de l'inverse.."<<std::endl;

    unsigned char normeChar[deuxPowm];
    unsigned char inverseChar[N];
    for(int i = 0; i < deuxPowm; i++){
        normeChar[i] = doubleToChar(norme[i]);
    }
    for(int i = 0; i < N; i++){
        inverseChar[i] = doubleToChar(partie_reelle[i]);
    }

    short channels_nb = MONO;                          // Nombre de canaux (1 pour mono ou 2 pour stéréo)
    int sampling_freq = freq;                    // Fréquence d'échantillonnage (en Hertz)(classique en musique : 44100 Hz)

    Wave sortantFourier = Wave(normeChar, deuxPowm, channels_nb, sampling_freq);
    std::cout<<"Création du Signal de norme de fourier"<<std::endl;
    sortantFourier.write(fourier);
    std::cout<<"Ecriture du fichier de norme de fourier"<<std::endl;

    Wave inverseWave =  Wave(inverseChar, N, channels_nb, sampling_freq);
    std::cout<<"Création du Signal inverse de fourier"<<std::endl;
    inverseWave.write(inverseNom);
    std::cout<<"Ecriture du fichier inverse"<<std::endl;
}

void Exo4(){
    std::cout<<"Entrez le nom du entrant.wav : "<<std::endl;
    char entre[30];
    std::cin>>entre;
    std::cout<<std::endl;

    std::cout<<"Entrez le nom du sortantFourierFiltreHaut.wav : "<<std::endl;
    char fourierHaut[30];
    std::cin>>fourierHaut;
    std::cout<<std::endl;

    std::cout<<"Entrez le nom du sortantPasseHaut.wav : "<<std::endl;
    char passhaut[30];
    std::cin>>passhaut;
    std::cout<<std::endl;

    std::cout<<"Entrez la valeur de la fréquance de coupure : "<<std::endl;
    int frequence_coupure;
    std::cin>>frequence_coupure;
    std::cout<<std::endl;

    std::cout<<"Enregistrement des paramètres ..."<<std::endl;

    Wave entrant = Wave();
    entrant.read(entre);

    unsigned char *signalEntrant; 
    int N;
    int freq;
    entrant.getData8(& signalEntrant, & N, & freq);
    int m = ceil(log2(N));
    int deuxPowm = pow(2,m);

    double partie_reelle[deuxPowm]; double partie_imaginaire[deuxPowm];
    for(int i = 0; i < N; i++){
        partie_reelle[i] = charToDouble(signalEntrant[i]);
        partie_imaginaire[i] = charToDouble(signalEntrant[i]);
    }
    for(int i = N; i < deuxPowm; i++){
        partie_reelle[i] = 0;
        partie_imaginaire[i] = 0;
    }
    std::cout<<"Mise en mémoire des valeures doubles du signal entrant.."<<std::endl;

    int fft = FFT(1, m, partie_reelle, partie_imaginaire);

    double normeHaut[deuxPowm];
    double reel_filtre[deuxPowm];
    double img_filtre[deuxPowm];
    
    passeHaut(img_filtre, reel_filtre, partie_reelle, partie_imaginaire, frequence_coupure, deuxPowm, freq);
    calculNorme(normeHaut, reel_filtre, img_filtre, deuxPowm);
    

    fft = FFT(-1, m, reel_filtre, img_filtre);
    
    std::cout<<"Calcule de la norme et de l'inverse.."<<std::endl;

    unsigned char normeHautChar[deuxPowm];
    unsigned char inverseChar[deuxPowm];
    for(int i = 0; i < deuxPowm; i++){
        normeHautChar[i] = doubleToChar(normeHaut[i]);
    }
    for(int i = 0; i < deuxPowm; i++){
        inverseChar[i] = doubleToChar(reel_filtre[i]);
    }

    short channels_nb = MONO;                          // Nombre de canaux (1 pour mono ou 2 pour stéréo)
    int sampling_freq = freq;                    // Fréquence d'échantillonnage (en Hertz)(classique en musique : 44100 Hz)

    Wave sortantFourier = Wave(normeHautChar, deuxPowm, channels_nb, sampling_freq);
    std::cout<<"Création du Signal de norme de fourier"<<std::endl;
    sortantFourier.write(fourierHaut);
    std::cout<<"Ecriture du fichier de norme de fourier"<<std::endl;

    Wave inverseWave =  Wave(inverseChar, N, channels_nb, sampling_freq);
    std::cout<<"Création du Signal inverse de fourier"<<std::endl;
    inverseWave.write(passhaut);
    std::cout<<"Ecriture du fichier inverse"<<std::endl;
}

void Exo4Coupe(){
    std::cout<<"Entrez le nom du entrant.wav : "<<std::endl;
    char entre[30];
    std::cin>>entre;
    std::cout<<std::endl;

    std::cout<<"Entrez le nom du sortantFourierFiltreHaut.wav : "<<std::endl;
    char fourierHaut[30];
    std::cin>>fourierHaut;
    std::cout<<std::endl;

    std::cout<<"Entrez le nom du sortantPasseHaut.wav : "<<std::endl;
    char passhaut[30];
    std::cin>>passhaut;
    std::cout<<std::endl;

    std::cout<<"Entrez la valeur de la fréquance de coupure 1: "<<std::endl;
    int frequence_coupure1;
    std::cin>>frequence_coupure1;
    std::cout<<std::endl;

    std::cout<<"Entrez la valeur de la fréquance de coupure 2: "<<std::endl;
    int frequence_coupure2;
    std::cin>>frequence_coupure2;
    std::cout<<std::endl;

    std::cout<<"Enregistrement des paramètres ..."<<std::endl;

    Wave entrant = Wave();
    entrant.read(entre);

    unsigned char *signalEntrant; 
    int N;
    int freq;
    entrant.getData8(& signalEntrant, & N, & freq);
    int m = ceil(log2(N));
    int deuxPowm = pow(2,m);

    double partie_reelle[deuxPowm]; double partie_imaginaire[deuxPowm];
    for(int i = 0; i < N; i++){
        partie_reelle[i] = charToDouble(signalEntrant[i]);
        partie_imaginaire[i] = charToDouble(signalEntrant[i]);
    }
    for(int i = N; i < deuxPowm; i++){
        partie_reelle[i] = 0;
        partie_imaginaire[i] = 0;
    }
    std::cout<<"Mise en mémoire des valeures doubles du signal entrant.."<<std::endl;

    int fft = FFT(1, m, partie_reelle, partie_imaginaire);

    double normeHaut[deuxPowm];
    double reel_filtre[deuxPowm];
    double img_filtre[deuxPowm];
    
    passeBande(img_filtre, reel_filtre, partie_reelle, partie_imaginaire, frequence_coupure1, frequence_coupure2, deuxPowm, freq);
    calculNorme(normeHaut, reel_filtre, img_filtre, deuxPowm);
    

    fft = FFT(-1, m, reel_filtre, img_filtre);
    
    std::cout<<"Calcule de la norme et de l'inverse.."<<std::endl;

    unsigned char normeHautChar[deuxPowm];
    unsigned char inverseChar[deuxPowm];
    for(int i = 0; i < deuxPowm; i++){
        normeHautChar[i] = doubleToChar(normeHaut[i]);
    }
    for(int i = 0; i < deuxPowm; i++){
        inverseChar[i] = doubleToChar(reel_filtre[i]);
    }

    short channels_nb = MONO;                          // Nombre de canaux (1 pour mono ou 2 pour stéréo)
    int sampling_freq = freq;                    // Fréquence d'échantillonnage (en Hertz)(classique en musique : 44100 Hz)

    Wave sortantFourier = Wave(normeHautChar, deuxPowm, channels_nb, sampling_freq);
    std::cout<<"Création du Signal de norme de fourier"<<std::endl;
    sortantFourier.write(fourierHaut);
    std::cout<<"Ecriture du fichier de norme de fourier"<<std::endl;

    Wave inverseWave =  Wave(inverseChar, N, channels_nb, sampling_freq);
    std::cout<<"Création du Signal inverse de fourier"<<std::endl;
    inverseWave.write(passhaut);
    std::cout<<"Ecriture du fichier inverse"<<std::endl;
}

void Exo5(){
    std::cout<<"Entrez le nom du entrant.wav : "<<std::endl;
    char entre[30];
    std::cin>>entre;
    std::cout<<std::endl;

    std::cout<<"Entrez le nom du sortantButterFiltre.wav : "<<std::endl;
    char butterS[30];
    std::cin>>butterS;
    std::cout<<std::endl;

    std::cout<<"Entrez le nom du FourierButter.wav : "<<std::endl;
    char fourier[30];
    std::cin>>fourier;
    std::cout<<std::endl;

    std::cout<<"Entrez la valeur de la fréquance de coupure : "<<std::endl;
    int frequence_coupure;
    std::cin>>frequence_coupure;
    std::cout<<std::endl;

    std::cout<<"Enregistrement des paramètres ..."<<std::endl;

    Wave entrant = Wave();
    entrant.read(entre);

    unsigned char *signalEntrant; 
    int N;
    int freq;
    entrant.getData8(& signalEntrant, & N, & freq);
    int m = ceil(log2(N));
    int deuxPowm = pow(2,m);

    double entrantDouble[N];
    for(int i = 0; i < N; i++){
        entrantDouble[i] = charToDouble(signalEntrant[i]);
    }
    
    double butterDouble[N];

    std::cout<<"Mise en mémoire des valeures doubles du signal entrant.."<<std::endl;

    butter(entrantDouble, butterDouble, N, freq, frequence_coupure);
    unsigned char butterChar[N];
    for(int i = 0; i < N; i++){
        butterChar[i] = doubleToChar(butterDouble[i]);
    }

    double partie_reelle[deuxPowm]; double partie_imaginaire[deuxPowm];
    for(int i = 0; i < N; i++){
        partie_reelle[i] = butterDouble[i];
        partie_imaginaire[i] = butterDouble[i];
    }
    for(int i = N; i < deuxPowm; i++){
        partie_reelle[i] = 0;
        partie_imaginaire[i] = 0;
    }

    double norme[deuxPowm];
    int fft = FFT(1, m, partie_reelle, partie_imaginaire);
    calculNorme(norme, partie_reelle, partie_imaginaire, deuxPowm);

    unsigned char butterFourier[deuxPowm];
    unsigned char normeChar[deuxPowm];

    for(int i = 0; i < deuxPowm; i++){
        normeChar[i] = doubleToChar(norme[i]);
    }

    short channels_nb = MONO;                          // Nombre de canaux (1 pour mono ou 2 pour stéréo)
    int sampling_freq = freq;                    // Fréquence d'échantillonnage (en Hertz)(classique en musique : 44100 Hz)

    Wave inverseWave =  Wave(butterChar, N, channels_nb, sampling_freq);
    std::cout<<"Création du Signal inverse de fourier"<<std::endl;
    inverseWave.write(butterS);
    std::cout<<"Ecriture du fichier inverse"<<std::endl;

    Wave sortantFourier = Wave(normeChar, deuxPowm, channels_nb, sampling_freq);
    std::cout<<"Création du Signal de norme de fourier"<<std::endl;
    sortantFourier.write(fourier);
    std::cout<<"Ecriture du fichier de norme de fourier"<<std::endl;
}

void Exo6(){
        std::cout<<"Entrez le nom du entrant.wav : "<<std::endl;
    char entre[30];
    std::cin>>entre;
    std::cout<<std::endl;

    std::cout<<"Entrez le nom du foruierTranspo.wav : "<<std::endl;
    char fourierHaut[30];
    std::cin>>fourierHaut;
    std::cout<<std::endl;

    std::cout<<"Entrez le nom du transpo.wav : "<<std::endl;
    char passhaut[30];
    std::cin>>passhaut;
    std::cout<<std::endl;

    std::cout<<"Entrez la valeur de la fréquance de transposition : "<<std::endl;
    int frequence_coupure;
    std::cin>>frequence_coupure;
    std::cout<<std::endl;

    std::cout<<"Enregistrement des paramètres ..."<<std::endl;

    Wave entrant = Wave();
    entrant.read(entre);

    unsigned char *signalEntrant; 
    int N;
    int freq;
    entrant.getData8(& signalEntrant, & N, & freq);
    int m = ceil(log2(N));
    int deuxPowm = pow(2,m);

    double partie_reelle[deuxPowm]; double partie_imaginaire[deuxPowm];
    for(int i = 0; i < N; i++){
        partie_reelle[i] = charToDouble(signalEntrant[i]);
        partie_imaginaire[i] = charToDouble(signalEntrant[i]);
    }
    for(int i = N; i < deuxPowm; i++){
        partie_reelle[i] = 0;
        partie_imaginaire[i] = 0;
    }
    std::cout<<"Mise en mémoire des valeures doubles du signal entrant.."<<std::endl;

    int fft = FFT(1, m, partie_reelle, partie_imaginaire);

    double normeHaut[deuxPowm];
    double reel_filtre[deuxPowm];
    double img_filtre[deuxPowm];
    
    transpo(img_filtre, reel_filtre, partie_reelle, partie_imaginaire, frequence_coupure, deuxPowm, freq);
    calculNorme(normeHaut, reel_filtre, img_filtre, deuxPowm);
    

    fft = FFT(-1, m, reel_filtre, img_filtre);
    
    std::cout<<"Calcule de la norme et de l'inverse.."<<std::endl;

    unsigned char normeHautChar[deuxPowm];
    unsigned char inverseChar[deuxPowm];
    for(int i = 0; i < deuxPowm; i++){
        normeHautChar[i] = doubleToChar(normeHaut[i]);
    }
    for(int i = 0; i < deuxPowm; i++){
        inverseChar[i] = doubleToChar(reel_filtre[i]);
    }

    short channels_nb = MONO;                          // Nombre de canaux (1 pour mono ou 2 pour stéréo)
    int sampling_freq = freq;                    // Fréquence d'échantillonnage (en Hertz)(classique en musique : 44100 Hz)

    Wave sortantFourier = Wave(normeHautChar, deuxPowm, channels_nb, sampling_freq);
    std::cout<<"Création du Signal de norme de fourier"<<std::endl;
    sortantFourier.write(fourierHaut);
    std::cout<<"Ecriture du fichier de norme de fourier"<<std::endl;

    Wave inverseWave =  Wave(inverseChar, N, channels_nb, sampling_freq);
    std::cout<<"Création du Signal inverse de fourier"<<std::endl;
    inverseWave.write(passhaut);
    std::cout<<"Ecriture du fichier inverse"<<std::endl;
}

int main(int argc, char** argv){   
    if(argc != 2){std::cout<<"Nombre incorrecte de paramètres: ./prog numéro de l'exercice"<<std::endl; return 0;}

    int exo = atoi(argv[1]);

    switch(exo)
    {
        case 1:
            Exo1();
            break;
        case 2: 
            Exo2();
            break;
        case 3: 
            Exo2FFT();
            break;
        case 4:
            Exo4();
            break;
        case 5: 
            Exo4Coupe();
            break;
        case 6: 
            Exo5();
            break;
        case 7: 
            Exo6();
            break;

        default:
        std::cout<<"Exercice non existant"<<std::endl;
    }
       
    return 0;
}